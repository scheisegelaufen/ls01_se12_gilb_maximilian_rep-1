
public class aufgaben {

	public static void main(String[] args) {
		
		//Aufgabe 1
		
		System.out.println("Das ist ein Beispielsatz. Ein Beispielsatz ist das.");
		
		System.out.println("Das ist ein \"Beispielsatz\". \n" + "Ein Beispielsatz ist das.");
		
		//Die println-Funktion erzeugt nach der Ausgabe zusätzlich einen Zeilenumbruch. Die print-Funktion macht dies nicht. Hier müsste dies mit dem Escape-Sympol n umgesetzt werden
		
		System.out.print("\n\n\n");			//um die Ausgabe der Konsole übersichtlicher zu machen
		
		
		
		//Aufgabe 2
		
		System.out.printf("%7s%n", "*");
		System.out.printf("%8s%n", "***");
		System.out.printf("%9s%n", "*****");
		System.out.printf("%10s%n", "*******");
		System.out.printf("%11s%n", "*********");
		System.out.printf("%12s%n", "***********");
		System.out.printf("%13s%n", "*************");
		System.out.printf("%8s%n", "***");
		System.out.printf("%8s%n", "***");
		
		System.out.print("\n\n\n");			//um die Ausgabe der Konsole übersichtlicher zu machen
		
		
		
		//Aufagbe 3
		
		double zahl1, zahl2, zahl3, zahl4, zahl5;
		
		zahl1 = 22.4234234;
		zahl2 = 11.2222;
		zahl3 = 4.0;
		zahl4 = 1000000.551;
		zahl5 = 97.43;
		
		System.out.printf( "%n%.2f %n%.2f %n%.2f %n%.2f %n%.2f" , zahl1, zahl2, zahl3, zahl4, zahl5);
		
		//stage-Kommentar, bitte ignorieren
		
	}

}
