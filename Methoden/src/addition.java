import java.util.Scanner;

public class addition {
	
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		String ersteZahl = "1. Zahl: ", zweiteZahl = "2. Zahl: ";

        //1.Programmhinweis
		addition.Programmhinweis();

        //4.Eingabe
        zahl1 = addition.Eingabe(ersteZahl);
        zahl2 = addition.Eingabe(zweiteZahl);

        //3.Verarbeitung
        erg = addition.Verarbeitung(zahl1, zahl2);

        //2.Ausgabe
        addition.Ausgabe(erg, zahl1, zahl2);
		    
	}
	
	
	
	public static void Programmhinweis(){
		System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
	}
	
	public static void Ausgabe(double a, double b, double c) {
		System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f + %.2f", a, b, c);
	}
	
	public static double Verarbeitung(double a, double b) {
		double c;
		c = a + b;
		return c;
	}
	
	public static double Eingabe (String text) {
		double a;
		System.out.println(text);
		a = sc.nextDouble();
		return a;
	}

}
